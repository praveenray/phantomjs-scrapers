var system = require('system');
var args = system.args;


var testindex = 0;
var loadInProgress = false;//This is set to true when a page is still loading
var holdings = [];

/*********SETTINGS*********************/
var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;
/*********SETTINGS END*****************/


console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

function click_for_holdings(portfolio_name) {
    console.log("Read Summary for "+portfolio_name);
    return page.evaluate(function (which_portfolio) {
        console.log('TITLE: '+document.title);
        var portfolio_link = null;
        $('a').each(function (i) {
            if (portfolio_link != null) return;
            var link = $(this);
            var text = (link.text() || '').trim().replace(/\s+/,' ');
            var href = link.attr('href');
            if (href && text.indexOf(which_portfolio) >= 0 && href.indexOf('/TFPHoldings/Holdings.aspx?') >= 0) {
                console.log("found "+which_portfolio);
                portfolio_link = link;
            }
        });
        if (portfolio_link != null) {
            console.log('clicking '+which_portfolio);
            portfolio_link[0].click();
            return true;
        }
        return false;
    }, portfolio_name);
}

function read_holdings() {
    // find <table summary='Table of Holdings By Account  for selected account'>//table/tr/td[0]
    console.log('reading holdings ');
    return page.evaluate(function () {
        console.log('sandbox url: ' + document.URL);
        var header_cells = $('table').find('thead tr th');
        var table = null;
        header_cells.each(function() {
           if (table) return;
           var th = $(this);
           if (th.text().indexOf('Symbol') >= 0) {
               table = th.parents('table');
           }
        });
        var trs = table.find('tbody tr');
        var size = trs.length;
        var symbols = [];
        var balances_found = false;
        for (var i=0; i < size; i++) {
            var tr = $(trs.get(i));
            var td = $(tr.find('td')[0]);
            var span = td.find('span').text().trim();
            if (span.indexOf('Balances') >= 0) {
                balances_found = true;
                break;
            }
            var ticker = td.find('a').text();
            ticker = ticker.replace(/\s+.*/,'').trim();
            var qty = Number($(tr.find('td').get(3)).text());
            if (!isNaN(qty) && qty > 0) {
                symbols.push(ticker);
            }
        }
        if (symbols.length > 0) {
            console.log('['); console.log(symbols); console.log(']');
            return ['holdings', symbols];
        } else if (balances_found) {
            return true; // come here is there are no holdings at all
        }
        return false;
    });
}

function pick_etrade_portfolio() {
    return page.evaluate(function () {
        if (!jQuery) return false;

        var portfolio_link = null;
        $('div.dropdown.open ul a').each(function (i, e) {
            if (portfolio_link) return;
            if ($(this).text() == 'Portfolios') {
                portfolio_link = $(this);
            }
        });
        if (portfolio_link) {
            portfolio_link.get(0).click();
            return true;
        }
        return false;
    });
}

function pick_etrade_portfolio_symbols() {
    return page.evaluate(function () {
        if (!jQuery) return false;
        var symbols = $('div[colid="symbol"]');
        if (symbols.length == 0) return false;
        var tickers = [];
        symbols.each(function (i, symbol) {
            var ticker = $(this).find('a').text().trim();
            if (ticker && ticker.length > 0) {
                tickers.push(ticker);
            }
        });
        console.log(tickers);
        return ['holdings', tickers];
    });
}

function click_on_etrade_portfolio_drop_down(which) {
    return page.evaluate(function (index) {
        if (!jQuery) return false;
        const btns = $('button#tour-quickLinks');
        if (btns.length == 0) return false;
        $(btns[index]).click();
        return true;
    }, which);
}

var steps_ml = [
    //Step 1 - Open ML home page
    function(){
        console.log('Step 1 - Open ML Login page');
        page.open("https://olui2.fs.ml.com/login/login.aspx", function(status){});
        return true;
    },
    //Step 3 - Populate and submit the login form
    function() {
        console.log('Populate and submit the login ML form');
        page.evaluate(function(u,p) {
            document.getElementById("txtUserid").value=u;
            document.getElementById("txtPassword").value=p;
            $("#btnValidate").click();
        }, ml_username, ml_password);
        return true;
    },

    function() {
        return click_for_holdings('IRA-Edge 53X-18A40');
    },
    function () {
        return read_holdings();
    },
    function () {
        page.goBack();
        return true;
    },
    function() {
        return click_for_holdings('CMA-Edge 28X-17352');
    },
    function () {
        return read_holdings();
    },
    function () {
        page.goBack();
        return true;
    },
    function() {
        return click_for_holdings('SEP-Edge 51X-41315');
    },
    function () {
        return read_holdings();
    }
];

var steps_stockcharts = [
    function () {
        console.log("go to stock charts login");
        page.open("https://stockcharts.com/scripts/php/dblogin.php", function () {});
        return true;
    },
    function () {
        console.log('logging into Stockcharts');
        return page.evaluate(function (u,p) {
            console.log('JQ: '+jQuery);
            if (!jQuery) return false;
            if ($('button[type="submit"]').length > 0) {
                $('input#form_UserID').val(u);
                $('input[name="form_UserPassword"]').val(p);
                $('button[type="submit"]').click();
                return true;
            }
            return false;
        }, stockcharts_username, stockcharts_password);
    },
    function () {
        console.log('goto chartlists');
        return page.evaluate(function () {
            if (!jQuery) return false;
            const chartlists = $('span#your-chartlists-text');
            if (chartlists.length == 0) return false;
            chartlists.click();
            return true;
        });
    },
    function () {
        console.log('looking for holdings and clicking to get drop down options');
        return page.evaluate(function () {
            if (!jQuery) return false;
            var holdings_link = null;
            $('td.cl-list-link a').each(function(i,e) {
                if (holdings_link) return;
                if ($(e).text() == 'holdings') holdings_link = $(e);
            });
            if (holdings_link == null) return false;
            const button = holdings_link.parents('tr').find('button.dropdown-toggle');
            if (button.length == 0) return false;
            button.click();
            return true;
        });
    },
    function () {
        console.log('pick edit list from the drop down');
        return page.evaluate(function () {
            if (!jQuery) return false;
            const dropdown = $('div.dropdown.open');
            if (dropdown.length == 0) return false;
            var edit_link = null;
            dropdown.find('a').each(function (i, e) {
                if (edit_link) return;
                if ($(e).text() == 'Edit List') {
                    edit_link = $(e);
                }
            });
            if (!edit_link) return false;
            edit_link.get(0).click();
            return true;
        });
    },
    function () {
        console.log('Finding and clicking select all or expecting empty list');
        return page.evaluate(function () {
            if (!jQuery) return false;
            if ($('div#newList-well').length != 0) {
                console.log('empty chartlist');
                return ['skip-by',4];
            }

            const select_all = $('a#selectAllCharts');
            console.log('select_all length: ' + select_all.length);
            if (select_all.length == 0) return false;
            select_all.click();
            return true;
        });
    },
    function () {
        console.log('waiting for all checkboxes to be checked');
        return page.evaluate(function () {
            if (!jQuery) return false;
            return $('input.tblchk[type="checkbox"]').is(':checked');
        });
    },
    function () {
        console.log('clicking on delete all');
        return page.evaluate(function () {
            if (!jQuery) return false;
            const delete_all = $('a#deleteCharts span i');
            if (delete_all.length == 0) return false;
            delete_all.click();
            return true;
        });
    },
    function () {
        console.log('waiting for delete to be done');
        return page.evaluate(function () {
            if (!jQuery) return false;
            return $('input.tblchk[type="checkbox"]').length == 0;
        });
    },
    function () {
        console.log('Finding add many options link');
        return page.evaluate(function () {
            if (!jQuery) return false;
            const add_options = $('input#type-add-many');
            if (add_options.length == 0) return false;
            add_options.click();
            return true;
        });
    },
    function () {
        return page.evaluate(function (holdings_to_add) {
            if (!jQuery) return false;
            const save = $('button#add-many-submit');
            const textarea = $('textarea#text_NewTickers');
            if (textarea.length == 0 || save.length == 0) return false;
            const map = {};
            holdings_to_add.forEach(function(item) {map[item]=1;});
            var unique_tickers = Object.keys(map);
            unique_tickers.sort();
            console.log('Adding Tickers '+unique_tickers);
            textarea.val(unique_tickers.join(','));
            save.click();
            return true;
        }, holdings);
    },
    function () {
        console.log('Waiting for save');
        return page.evaluate(function () {
            if (!jQuery) return false;
            return $('input.tblchk[type="checkbox"]').length > 0;
        });
    }
];

var etrade_steps = [
    function () {
        console.log('goto etrade login');
        page.open("https://us.etrade.com/home/welcome-back", function(status){});
        return true;
    },
    function () {
        console.log("logging into Etrade");
        return page.evaluate(function(u,p) {
            if (!jQuery) return false;
            var username = $('input#user_orig');
            if (username.length == 0) return false;

            $('input#user_orig').val(u);
            $('input[name="PASSWORD"]').val(p);
            $('button[type="submit"]').click();
            return true;
        }, etrade_username, etrade_password);
    },
    function () {
        console.log('on accounts screen');
        return click_on_etrade_portfolio_drop_down(0);
    },
    function () {
        console.log('dropdown open. Looking for Portfolio');
        return pick_etrade_portfolio();
    },
    function () {
        console.log('Going to portfolio. Picking Symbols');
        return pick_etrade_portfolio_symbols();
    },
    function () {
        page.goBack();
        return true;
    },
    function () {
        console.log('back on portfolio');
        return click_on_etrade_portfolio_drop_down(1);
    },
    function () {
        console.log('second dropdown open. Looking for Portfolio');
        return pick_etrade_portfolio();
    },
    function () {
        console.log('Picking Symbols again');
        return pick_etrade_portfolio_symbols();
    }
];


/**********END STEPS THAT FANTOM SHOULD DO***********************/

//Execute steps one by one
var steps = steps_ml.concat(etrade_steps).concat(steps_stockcharts).concat([
    function () {
        return ['done'];
    }
]);

var ml_username = args[1]; var ml_password = args[2];
var etrade_username = args[3]; var etrade_password = args[4];
var stockcharts_username = args[5]; var stockcharts_password=args[6];

var interval = setInterval(executeRequestsStepByStep,100);

function executeRequestsStepByStep(){
    if (loadInProgress == false && typeof steps[testindex] == "function") {
        var result = steps[testindex]();
        if (typeof(result) == 'object') {
            const type = result[0];
            switch(type) {
                case "holdings":
                    holdings = holdings.concat(result[1]);
                    result = true;
                    break;
                case "skip-by":
                    testindex += result[1] - 1;
                    console.log('skipped to ' + (testindex+1));
                    result = true;
                    break;
                case 'done':
                    console.log('DONE');
                    phantom.exit(0);
                    break;
            }
        }
        if (result) {
            testindex++;
        }
    }
    if (typeof steps[testindex] != "function") {
        console.log("End Reached");
        phantom.exit();
    }
}

console.log("****HOLDINGS****");
console.log(holdings);

/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onLoadStarted = function() {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function() {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
page.onConfirm=function (msg) {
    console.log('confirm : ' + msg);
    return msg == 'Are you sure you want to permanently delete the selected charts?';
};
